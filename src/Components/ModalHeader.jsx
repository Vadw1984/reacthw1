import React, {Component} from 'react';
import ButtonModalHeader from './ButtonModalHeader.jsx'
import styles from '../styleMemu.css'


class ModalHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        // console.log(this.props, 'PropsOfModal');
        // console.log(this.props.styleBgModal);
        return (

                <div
                    style={{
                        background: this.props.styleBgModal,
                        width: '30%',
                        margin: '0 auto',
                        // marginTop: '10%',
                        borderRadius: '10px',
                        zIndex:'100'
                    }}>
                    <div style={{background: 'RGBA(0,0,0,0.3)', borderRadius: '10px'}}>
                        <div className="flexClass modalText"
                             style={{justifyContent: 'space-around', alignItems: 'center'}}>
                            <p>{this.props.headerModalText}</p>

                            <ButtonModalHeader
                                functionClickState={this.props.functionClickState}
                                functionDefindTarget={this.props.functionDefindTarget}
                                name="secondButton"
                                textButton='X'
                                styleBgButton='RGBA(0,0,0,0.3)'
                                styleBorRadButton='7px'>

                            </ButtonModalHeader>
                        </div>
                    </div>

                    <p className="flexClass modalText">{this.props.textModal}</p>

                    <div className="flexClass" style={{justifyContent: "space-around"}}>
                        <ButtonModalHeader functionClickState={this.props.functionClickState}
                                           functionDefindTarget={this.props.functionDefindTarget}
                                           textButton='Ok'
                                           styleBgButton='RGBA(0,0,0,0.3)'
                                           styleBorRadButton='7px'
                                           height='30px'
                                           width='100px'
                        >
                        </ButtonModalHeader>
                        <ButtonModalHeader functionClickState={this.props.functionClickState}
                                           functionDefindTarget={this.props.functionDefindTarget}
                            // onClick={this.props.functionClickState}
                                           textButton='Cancel'
                                           styleBgButton='RGBA(0,0,0,0.3)'
                                           styleBorRadButton='7px'
                                           marginButton='10px'
                                           borderButton='none'
                                           height='30px'
                                           width='100px'
                        >
                        </ButtonModalHeader>

                    </div>
                </div>

        );
    }
}

export default ModalHeader;