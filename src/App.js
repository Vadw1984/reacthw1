import React, { Component } from "react";
import ModalHeader from "./Components/ModalHeader.jsx";
import ButtonModalHeader from "./Components/ButtonModalHeader.jsx";
import Obfuscate from "./Components/Obfuscate";

import "./App.css";

class App extends Component {
  state = {
    isClickedBtn: false,
    headerNameFirstButton: "Open first modal",
    headerNameSecondButton: "Open second modal",
    backGroundModal: "",
    backGroundFirstModal: "red",
    backGroundSecondModal: "green"
  };

  clickState = () => {
    this.setState(prevState => ({
      isClickedBtn: !prevState.isClickedBtn
    }));
  };
  defindClickTarget = (event) => {
    console.log(event.target.name === 'secondButton');

    if (event.target.name === 'firstButton') {
        this.headerModalText = 'Do you want to delete this file?'
        this.modalText = 'Once you delete this file, it won’t be possible to undo this action. \n' +
            'Are you sure you want to delete it?'
        this.backGroundModal = 'red'
        console.log('11111');
    } else {
        this.headerModalText = 'Do you want to confirm Your choice?'
        this.modalText = 'Are you Ready to confirm Your choice?'
        this.backGroundModal = 'green'
        console.log('2222222222');
    }

}

  render() {
    const { isClickedBtn } = this.state;

    return (
      <div className="App">
        {isClickedBtn && ( 
          <Obfuscate
            functionDefindTarget={this.defindClickTarget}
            functionClickState={this.clickState}
          />
        )}

        <ButtonModalHeader
          name="firstButton"
          functionDefindTarget={this.defindClickTarget}
          functionClickState={this.clickState}
          textButton={this.state.headerNameFirstButton}
          styleBgButton={this.state.backGroundFirstModal}
          marginButton="10px"
          styleBorRadButton="7px"
        ></ButtonModalHeader>

        <ButtonModalHeader
          name="secondButton"
          functionDefindTarget={this.defindClickTarget}
          functionClickState={this.clickState}
          textButton={this.state.headerNameSecondButton}
          styleBgButton={this.state.backGroundSecondModal}
          marginButton="10px"
          styleBorRadButton="7px"
        ></ButtonModalHeader>

        {isClickedBtn && (
          <ModalHeader
            functionDefindTarget={this.defindClickTarget}
            functionClickState={this.clickState}
            headerModalText={this.headerModalText}
            textModal={this.modalText}
            styleBgModal={this.backGroundModal}
          />
        )}
      </div>
    );
  }
}

export default App;
